import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-image',
  templateUrl: './new-image.component.html',
  styleUrls: ['./new-image.component.css']
})
export class NewImageComponent implements OnInit {

  @ViewChild('someVar') input: ElementRef;

  show: boolean = false;
  imageSrc: string;
  imageName: string;
  fromY: number;
  toY: number;
  fromX: number;
  toX: number;
  toolTip: string;

  router: Router;
  activateRoute: ActivatedRoute;
  
  constructor(
    router: Router,
    activateRoute: ActivatedRoute
  ) {
    this.router = router;
    this.activateRoute = activateRoute;
   }

  ngOnInit() {
    this.activateRoute.queryParams.subscribe(params => {
      if(params['data']) {
        
        let data = JSON.parse(params['data']);
        this.imageSrc = data.img;
        this.imageName = data.name;
        this.fromX = data.fromX;
        this.toX = data.toX;
        this.fromY = data.fromY;
        this.toY = data.toY;
        let check = data.toolTip;
        this.toolTip = check;
        // this.input.nativeElement.focus();
        // this.input.nativeElement.title = this.toolTip;
        console.log(this.toolTip);
      }
    })
  }

  onFileChanged(event) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      // console.log(event);
      this.imageName = event.target.files[0].name;
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.imageSrc = reader.result;
        // this.input.nativeElement.focus();
        // this.input.nativeElement.title = this.toolTip;
      }
    }
  }

  imageClick(event) {
    this.fromX = event.clientX-20;
    this.toX = event.clientX+20;
    this.fromY = event.clientY-20;
    this.toY = event.clientY+20;
  }

  onUpload() {
    if(!this.imageSrc) {
      alert('Choose photo');
    }
    else if(!this.toolTip) {
      alert('Input toopTip first');
    }
    else if(!this.fromX && !this.fromY) {
      alert('Tap on image to set pointer')
    }
    else {
      let obj = {
        img: this.imageSrc,
        name: this.imageName,
        fromX: this.fromX,
        toX: this.toX,
        fromY: this.fromY,
        toY: this.toY,
        toolTip: this.toolTip
      };
      if(localStorage.getItem('images')) {
        let imageList = JSON.parse(localStorage.getItem('images'));
        imageList.push(obj);
        localStorage.setItem('images', JSON.stringify(imageList));
      }
      else {
        let imageList = [];
        imageList.push(obj);
        localStorage.setItem('images', JSON.stringify(imageList));
      }
      this.router.navigateByUrl('/images');
    }
    
  }

  coordinates(event) {
    if((event.clientX >= this.fromX && event.clientX <= this.toX) && (event.clientY >= this.fromY && event.clientY <= this.toY)) {

      console.log('TRUE');
      this.show = true;
    }
    else {
      this.show = false;
    }
  }
}
