import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {

  images: any;
  router: Router;
  constructor(
    router: Router
  ) {
    this.router = router;
   }

  ngOnInit() {
    if(localStorage.getItem('images')) {
      this.images = JSON.parse(localStorage.getItem('images'));
      console.log(this.images);
    }
    else {
      
    }
  }

  editImage(data) {
    if(data) {
      console.log('DATA');
      console.log(data);
      let rout = '/new'
      this.router.navigate(['/new'], {queryParams: {data: JSON.stringify(data)}})
    }
    else {
      this.router.navigateByUrl('/new');
    }
  }
}
