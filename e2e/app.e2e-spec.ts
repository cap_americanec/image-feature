import { ImageFeaturePage } from './app.po';

describe('image-feature App', function() {
  let page: ImageFeaturePage;

  beforeEach(() => {
    page = new ImageFeaturePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
